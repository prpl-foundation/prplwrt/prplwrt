Create R alias:

  $ alias R="${CRAM_REMOTE_COMMAND:-}"

Check that miniupnpd is enabled and running by default:

  $ R "pgrep --count miniupnpd"
  1

  $ R "ubus -S call UPnP.Device _get '{\"rel_path\":\"UPnPIGD\"}'"
  {"UPnP.Device.":{"UPnPIGD":true}}
  {}
  {"amxd-error-code":0}

  $ R "netstat -tulpn | grep miniupnpd | grep tcp"
  tcp        0      0 :::5000                 :::\*                    LISTEN      .*\/miniupnpd (re)

  $ R "netstat -tulpn | grep miniupnpd | grep 1900 | sort"
  udp        0      0 0.0.0.0:1900            0.0.0.0:\*                           .*\/miniupnpd (re)
  udp        0      0 :::1900                 :::\*                                .*\/miniupnpd (re)

  $ R "netstat -tulpn | grep miniupnpd | grep 5351"
  udp        0      0 192.168.1.1:5351        0.0.0.0:\*                           .*\/miniupnpd (re)
  udp        0      0 :::5351                 :::\*                                .*\/miniupnpd (re)

Disable miniupnpd:

  $ R "ubus -S call UPnP.Device _set '{\"parameters\":{\"UPnPIGD\":False}}'" ; sleep 2
  {"UPnP.Device.":{"UPnPIGD":false}}
  {}
  {"amxd-error-code":0}

Check that miniupnpd is disabled and not running:

  $ R "pgrep --count miniupnpd"
  0
  [1]

  $ R "ubus -S call UPnP.Device _get '{\"rel_path\":\"UPnPIGD\"}'"
  {"UPnP.Device.":{"UPnPIGD":false}}
  {}
  {"amxd-error-code":0}

  $ R "netstat -tulpn | grep miniupnpd"
  [1]

Enable miniupnpd:

  $ R "ubus -S call UPnP.Device _set '{\"parameters\":{\"UPnPIGD\":True}}'" ; sleep 2
  {"UPnP.Device.":{"UPnPIGD":true}}
  {}
  {"amxd-error-code":0}

Check that miniupnpd is enabled and running again:

  $ R "pgrep --count miniupnpd"
  1

  $ R "ubus -S call UPnP.Device _get '{\"rel_path\":\"UPnPIGD\"}'"
  {"UPnP.Device.":{"UPnPIGD":true}}
  {}
  {"amxd-error-code":0}

  $ R "netstat -tulpn | grep miniupnpd | grep tcp"
  tcp        0      0 :::5000                 :::\*                    LISTEN      .*\/miniupnpd (re)

  $ R "netstat -tulpn | grep miniupnpd | grep 1900 | sort"
  udp        0      0 0.0.0.0:1900            0.0.0.0:\*                           .*\/miniupnpd (re)
  udp        0      0 :::1900                 :::\*                                .*\/miniupnpd (re)

  $ R "netstat -tulpn | grep miniupnpd | grep 5351"
  udp        0      0 192.168.1.1:5351        0.0.0.0:\*                           .*\/miniupnpd (re)
  udp        0      0 :::5351                 :::\*                                .*\/miniupnpd (re)
